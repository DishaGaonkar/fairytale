[![Build Status](https://travis-ci.org/schnaader/fairytale.svg?branch=master)](https://travis-ci.org/schnaader/fairytale)
[![Build status](https://ci.appveyor.com/api/projects/status/k3y23dpxfu4rm108?svg=true)](https://ci.appveyor.com/project/schnaader/fairytale)
[![Join the chat at https://gitter.im/encode-ru-Community-Archiver/Lobby](https://badges.gitter.im/encode-ru-Community-Archiver/Lobby.svg)](https://gitter.im/encode-ru-Community-Archiver/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

# Table of Contents

-Fairytale
-How to build
-How to contribute
-Releases/Binaries
-Licenses

# Fairytale

Fairytale is a modern lossless community archiver, which allows for data to be archived and compressed, that features:

- state of the art analysis of input data
- detection and transform / recompression of data to improve compression, which means that lossless compression reduces bits by identifying and eliminating statistical redundancy.
- sorting input and arrange it in different streams
- deduplication across all streams and recursion levels
- applying different (best fit) compression algorithms on each stream
- storing compressed data in a modern archiver format

Some important terminology to know:

- Deduplication is the removal of excessive copies of data which significantly decreases storage capacity requirements.
- Compression algorithms reduce the number of bytes required to represent data and reduced the amount of memory required to store data.
- To learn more on how to store compressed data in an archiver format, follow [this](https://www.ionos.com/digitalguide/server/tools/archiving-and-compression-using-linux/) link

It offers great modularity so that any algorithm can be added to it
and enough flexibility to chose between fast, best practical or experimental state-of-the-art compression.
Fairytale is a dream of a next generation archiver and it is a work in progress
so if you share our dream and want to contribute, [join our great community here](https://gitter.im/encode-ru-Community-Archiver)

## How to build

Using CMake (download it [here](https://cmake.org/download/)), you can build on many platforms using your favorite compiler (Visual Studio, MinGW, CodeBlocks, XCode, Unix Makefiles, ...). It will also detect automatically if zlib is installed and if not, compiles it from source.

After following the link, choose which platform to download Cmake on. (i.e Windows, Linux, or Unix).

After downloading, follow the instructions that the installer gives. After the application is finished installing, open the application and add the source files and click generate. This will create the files needed to run the program.

Zlib is a software library used for data compression. It contains encapsulation, algorithms, resource use, strategies, and error handling, and there is no limit to the length of data that can be compressed.

download zlib [here](https://www.zlib.net/)

## How to contribute

Welcome to Fairytale! [Connect with us on glitter](https://gitter.im/encode-ru-Community-Archiver).

- contributors should avoid plagiarizing and the work complies with the coding standards seen in the files.
- avoid unnecessary and redundant code and try to keep the code as clean as possible.
- add insightful comments when needed and make the comments clear and concise
- use C# or c++ when coding.
- be respectful and help others when possible
- review the codebase before contributing
- understand the terminology and what the purpose of this repository is
- Implement thorough testing before doing a pull request.
- If fixing a bug, specify what the bug was in the pull request and give a thorough description of what was fixed.
- Coding style should be professional, focus on naming conventions of functions, variables, and classes and add comments when needed.
- limit grammar mistakes and unclear coding conventions.
- avoid massive changes to the code base and do frequent commits and saves
- when creating a pull request, remember to be specific and descriptive so others know what exactly you did
- merge pull requests once approved.

## Releases/Binaries

Please note that this is a very rough prototype that allows for testing of the pre-processing library.
It doesn't apply any compression right now.
Request access by following the google drive link as provided. Currently, there isn't a release for ARM and OSX

[for ARM](https://drive.google.com/file/d/1Uc1w3Sf0J8A2wGZtcYtIDpHjcSuX8oY7/view)

[for Linux](..)

[for OSX](..)

[for Windows](https://drive.google.com/drive/folders/1uj2YVjpbRscJiM0llTU-9uJuY5BmgBvt)

## License

Licensed under the [LGPL-3.0 license](https://gitlab.com/schnaader/fairytale/blob/master/LICENSE)
